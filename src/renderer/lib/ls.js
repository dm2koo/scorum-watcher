import store from 'store';

const SETTINGS_KEY = 'settings';
const FOLLOWS_KEY = 'follows';

export const getSettings = () => store.get(SETTINGS_KEY) || {};
export const setSettings = settings => store.set(SETTINGS_KEY, { ...getSettings(), ...settings });

export const getFollows = () => store.get(FOLLOWS_KEY) || null;
export const setFollows = follows => store.set(FOLLOWS_KEY, follows);
