import { Component } from 'react';
import PropTypes from 'prop-types';
import { flatten } from 'lodash';
import createDebug from 'debug';
import { getRecentPosts, getHotPosts } from '../../../common/api/scorum';
import { connect } from '../../store/store';
import { getSettings } from '../../lib/ls';
import { timaAgo, getPostId, getDiffMinutes, isUserVoted, getPostSp, getPostUrl } from '../../../common/utils/utils';
import { HOT_POSTS_LIMIT, RECENT_POSTS_LIMIT } from '../../../common/constants';
import initialState from '../../store/initial-state';

const shownRecentNotifications = [];
const shownHotNotifications = [];
const debug = createDebug('watcher');

const showPostNotification = (post, isHot) => {
  const metaData = JSON.parse(post.json_metadata || {});
  const prefix = isHot ? '🔥 HOT' : '📄 New';
  const postId = getPostId(post);

  const notif = new Notification(`${prefix}: ${post.author} ${timaAgo(post.created)}`, {
    body: post.title,
    icon: `${metaData.image}_notifications_thumb`,
  });

  notif.onclick = () => {
    window.electron.shell.openExternal(getPostUrl(post.url, metaData.domains[0]));
    window.setTrayIconWatching();
  };

  if (isHot) {
    shownHotNotifications.push(postId);
  } else {
    shownRecentNotifications.push(postId);
  }

  window.setTrayIconBadge();
};

class WatcherUI extends Component {
  static propTypes = {
    follows: PropTypes.arrayOf(PropTypes.string).isRequired,
  };

  watcher;
  settings = {};

  componentDidMount() {
    this.settings = { ...initialState.settings, ...getSettings() };
    const { interval } = this.settings;

    if (!interval) { return; }

    window.sendWatchingStatus(true);

    debug('Started');

    this.watcher = setInterval(this.getPostsForDomains, Number(interval) * 1000 * 60);
    this.getPostsForDomains();
  }

  componentWillUnmount() {
    this.clearWatcher();
  }

  clearWatcher = () => {
    if (!this.watcher) { return; }

    clearInterval(this.watcher);
    this.watcher = null;
    window.sendWatchingStatus(false);
    debug('Stopped');
  }

  getPostsForDomains = () => {
    const { domains } = this.settings;

    if (!this.watcher) { return; }

    const recentPromises = domains.length > 0 ?
      domains.map(domain => getRecentPosts({ limit: RECENT_POSTS_LIMIT, tags: [domain] })) :
      [getRecentPosts({ limit: RECENT_POSTS_LIMIT })];

    const hotPromises = domains.length > 0 ?
      domains.map(domain => getHotPosts({ limit: HOT_POSTS_LIMIT, tags: [domain] })) :
      [getHotPosts({ limit: HOT_POSTS_LIMIT })];

    Promise.all(recentPromises).then(lists => this.showRecentPostsNotifications(flatten(lists)));
    Promise.all(hotPromises).then(lists => this.showHotPostsNotifications(flatten(lists), true));

    debug('Watching request...');
  }

  isShowRecentPostNotification = (post) => {
    const { follows } = this.props;
    const diff = getDiffMinutes(post.created);
    const { username, recentMinutesMin, recentMinutesMax } = this.settings;

    return diff >= Number(recentMinutesMin) && diff <= Number(recentMinutesMax) &&
      follows.includes(post.author) &&
      !shownRecentNotifications.includes(getPostId(post)) &&
      !isUserVoted(post.active_votes, username);
  };

  isShowHotPostNotification = (post) => {
    const diff = getDiffMinutes(post.created);
    const { username, hotMinutesMin, hotMinutesMax, hotSpMin } = this.settings;

    return diff >= Number(hotMinutesMin) && diff <= Number(hotMinutesMax) &&
      getPostSp(post) > Number(hotSpMin) &&
      !shownHotNotifications.includes(getPostId(post)) &&
      !isUserVoted(post.active_votes, username);
  };

  showRecentPostsNotifications = (posts) => {
    posts.forEach((post) => {
      if (this.isShowRecentPostNotification(post)) {
        showPostNotification(post);
      }
    });
  };

  showHotPostsNotifications = (posts) => {
    posts.forEach((post) => {
      if (this.isShowHotPostNotification(post)) {
        showPostNotification(post, true);
      }
    });
  };

  render() {
    return null;
  }
}

const mapStateToProps = ({ follows }) => ({
  follows,
});

export const Watcher = connect(mapStateToProps)(WatcherUI);
