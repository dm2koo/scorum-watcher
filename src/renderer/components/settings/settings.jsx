import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { remove } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import { isUsernameValid } from '../../../common/utils/utils';
import { getAccount } from '../../../common/api/scorum';
import { connect, actions } from '../../store/store';
import { DOMAINS } from '../../../common/constants';

const styles = theme => ({
  header: {
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  container: {
    padding: theme.spacing.unit * 3,
  },
  paddingBottom: {
    paddingBottom: theme.spacing.unit * 3,
  },
  flex: {
    flexGrow: 1,
  },
  row: {
    flexWrap: 'nowrap',
  },
  rowInput: {
    flexGrow: 1,
    marginRight: theme.spacing.unit * 2,
  },
  changeSettings: {
    textAlign: 'center',
  },
  switchBar: {
    backgroundColor: '#fff',
  },
  disabledSwitchBar: {
    backgroundColor: '#ccc',
  },
});

const TOOLTIP_TEXTS = {
  USERNAME: 'Please provide a username',
  DOMAIN: 'Please provide at least one domain',
  BOTH: 'Please provide a username and at least one domain',
};

const SETTINGS_MIN_VALUES = {
  interval: 0.5,
  recentMinutesMin: 1,
  recentMinutesMax: 1,
  hotMinutesMin: 1,
  hotMinutesMax: 1,
  hotSpMin: 1,
};

const validateValue = value => /^\d+.?\d{0,9}$/.test(value) && !/^0\d/.test(value);

export class SettingsUI extends Component {
  static propTypes = {
    enabled: PropTypes.bool.isRequired,
    username: PropTypes.string,
    recentMinutesMin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    recentMinutesMax: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    hotMinutesMax: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    hotMinutesMin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    hotSpMin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    interval: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    followsForUsername: PropTypes.string,
    classes: PropTypes.shape().isRequired,
    domains: PropTypes.arrayOf(PropTypes.string).isRequired,
    isOnline: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    username: '',
    followsForUsername: '',
  };

  state = {
    usernameError: null,
    domainsError: null,
    isUsernameChecking: false,
    username: this.props.username,
  };

  componentWillMount() {
  }

  onUsernameChange = (e) => {
    const { usernameError } = this.state;

    if (usernameError) {
      this.setState({ usernameError: null });
    }

    this.setState({ username: e.target.value.trim() });
  }

  onUsernameBlur = async () => {
    const { username } = this.state;

    if (!isUsernameValid(username) && username.length) {
      this.setState({ usernameError: 'Invalid username format' });
      return;
    }

    actions.setUsername(username);
  }

  toggleDomain = (e) => {
    const { domains } = this.props;
    const { domainsError } = this.state;
    const newDomains = domains.slice();
    const { checked, value } = e.target;

    if (checked) {
      newDomains.push(value);
    } else {
      remove(newDomains, item => item === value);
    }

    if (!newDomains.length) {
      this.setState({ domainsError: TOOLTIP_TEXTS.DOMAIN });
    } else if (domainsError) {
      this.setState({ domainsError: null });
    }

    actions.setDomains(newDomains);
  }

  toggleWatching = async (e) => {
    const { followsForUsername } = this.props;
    const { username } = this.state;
    const value = e.target.checked;

    if (value && !isUsernameValid(username) && username.length) {
      this.setState({ usernameError: 'Invalid username format' });
      return;
    }

    actions.toggleEnable(value);

    if (!value) { return; }

    this.setState({ isUsernameChecking: true });
    const accounts = await getAccount([username]);
    this.setState({ isUsernameChecking: false });

    if (!accounts.length) {
      this.setState({ usernameError: 'Username does not exist' });
      actions.toggleEnable(false);
      return;
    }

    if (followsForUsername !== username) {
      actions.loadFollows(username);
    }
  }

  onInputChange = (e) => {
    const { id, value } = e.target;

    if (value.length && !validateValue(value)) {
      return;
    }

    actions.setInputValue(id, value.replace('-', '', 'g') || '');
  }

  onInputBlur = (e) => {
    const { id, value } = e.target;
    const num = Number(value);

    if (num < SETTINGS_MIN_VALUES[id]) {
      actions.setInputValue(id, SETTINGS_MIN_VALUES[id]);
    }
  }

  renderDomains = () => {
    const { domains } = this.props;

    return DOMAINS.map((domain) => {
      const value = `domain-${domain}`;
      const isChecked = domains.includes(value);

      return (
        <FormControlLabel
          key={value}
          control={(
            <Checkbox
              checked={isChecked}
              onChange={this.toggleDomain}
              value={value}
              color="primary"
            />
          )}
          label={domain}
        />
      );
    });
  }

  render() {
    const {
      classes, username, enabled, domains, recentMinutesMin, recentMinutesMax,
      hotMinutesMin, hotMinutesMax, hotSpMin, interval, isOnline,
    } = this.props;
    const { usernameError, isUsernameChecking, domainsError } = this.state;
    const isNoDomains = !domains.length;
    const isNoUsername = !username || !!usernameError;
    const isToggleDisabled = isNoUsername || isNoDomains;
    let tooltipMsg = '';

    if (isNoUsername && isNoDomains) {
      tooltipMsg = TOOLTIP_TEXTS.BOTH;
    } else if (isNoUsername) {
      tooltipMsg = TOOLTIP_TEXTS.USERNAME;
    } else if (isNoDomains) {
      tooltipMsg = TOOLTIP_TEXTS.DOMAIN;
    }

    return (
      <Fragment>
        <AppBar position="static" color="primary">
          <Toolbar className={classes.header}>
            <Typography variant="title" color="inherit" className={classes.flex}>
              SCORUM WATCHER
            </Typography>
            {!isOnline && <Typography color="error" className={classes.flex}>offline</Typography>}

            <div style={{ width: 16 }}>
              {isUsernameChecking && <CircularProgress size={16} style={{ color: '#fff' }} />}
            </div>

            <Tooltip
              title={tooltipMsg}
              placement="bottom"
              disableHoverListener={!isToggleDisabled}
              disableTouchListener={!isToggleDisabled}
              disableFocusListener={!isToggleDisabled}
            >
              <span>
                <Switch
                  checked={enabled}
                  onChange={this.toggleWatching}
                  disabled={isToggleDisabled}
                  classes={{
                    bar: classes.switchBar,
                  }}
                />
              </span>
            </Tooltip>
          </Toolbar>
        </AppBar>

        {enabled && (
          <FormHelperText className={classes.changeSettings}>
            To change your settings turn off the watching
          </FormHelperText>)}

        <div className={classes.container}>
          <FormGroup row className={`${classes.row} ${classes.paddingBottom}`}>
            <FormControl
              fullWidth
              error={!!usernameError}
              aria-describedby="name-error-text"
              disabled={enabled}
              className={classes.rowInput}
            >
              <InputLabel htmlFor="settings-username">Username</InputLabel>
              <Input
                id="settings-username"
                value={this.state.username}
                onChange={this.onUsernameChange}
                onBlur={this.onUsernameBlur}
              />
              {usernameError && <FormHelperText id="name-error-text">{usernameError}</FormHelperText>}
            </FormControl>

            <TextField
              id="interval"
              label="Interval"
              value={interval}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              type="number"
              disabled={enabled}
            />
          </FormGroup>

          <FormControl error={domainsError} disabled={enabled} className={classes.paddingBottom}>
            <FormLabel component="legend">Select domains</FormLabel>
            <FormGroup row>
              {this.renderDomains()}
            </FormGroup>
            {domainsError && <FormHelperText>{domainsError}</FormHelperText>}
          </FormControl>

          <FormLabel component="legend" disabled={enabled}>Recent posts settings</FormLabel>
          <FormGroup row className={`${classes.row} ${classes.paddingBottom}`}>
            <TextField
              id="recentMinutesMin"
              label="Min. minutes"
              value={recentMinutesMin}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              className={classes.rowInput}
              type="number"
              margin="normal"
              disabled={enabled}
            />
            <TextField
              id="recentMinutesMax"
              label="Max. minutes"
              value={recentMinutesMax}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              className={classes.flex}
              type="number"
              margin="normal"
              disabled={enabled}
            />
          </FormGroup>

          <FormLabel component="legend" disabled={enabled}>Hot posts settings</FormLabel>
          <FormGroup row className={classes.row}>
            <TextField
              id="hotMinutesMin"
              label="Min. minutes"
              value={hotMinutesMin}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              className={classes.rowInput}
              type="number"
              margin="normal"
              disabled={enabled}
            />
            <TextField
              id="hotMinutesMax"
              label="Max. minutes"
              value={hotMinutesMax}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              className={classes.rowInput}
              type="number"
              margin="normal"
              disabled={enabled}
            />
            <TextField
              id="hotSpMin"
              label="Min. SP"
              value={hotSpMin}
              onChange={this.onInputChange}
              onBlur={this.onInputBlur}
              className={classes.flex}
              type="number"
              margin="normal"
              disabled={enabled}
            />
          </FormGroup>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ follows, followsForUsername, settings: {
  username, enabled, domains, recentMinutesMin, recentMinutesMax, hotMinutesMin, hotMinutesMax, hotSpMin, interval,
} }) => ({
  username,
  enabled,
  follows,
  followsForUsername,
  domains,
  recentMinutesMin,
  recentMinutesMax,
  hotMinutesMin,
  hotMinutesMax,
  hotSpMin,
  interval,
});

export const Settings = withStyles(styles)(connect(mapStateToProps)(SettingsUI));
