import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { withStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import { Settings } from './components/settings/settings';
import { initMainConfig } from '../common/config/main';
import { initSideConfig } from '../common/config/side';
import { Provider, connect, actions } from './store/store';
import { Watcher } from './components/watcher/watcher';

const myTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#1e88e5',
    },
    secondary: {
      main: '#00e676',
    },
  },
});

const styles = {
  offline: {
    textAlign: 'center',
    position: 'absolute',
    top: 8,
    color: red[900],
    width: '100%',
  },
};

class AppUI extends Component {
  static propTypes = {
    follows: PropTypes.arrayOf(PropTypes.string),
    username: PropTypes.string,
    enabled: PropTypes.bool.isRequired,
    classes: PropTypes.shape().isRequired,
  };

  static defaultProps = {
    follows: [],
    username: '',
  };

  state = {
    isOnline: navigator.onLine,
  }

  constructor(props) {
    super(props);
    initMainConfig();
    initSideConfig();
  }

  isWacherEnabled = () => {
    const { username, follows, enabled } = this.props;
    const { isOnline } = this.state;

    return follows.length > 0 && username && enabled && isOnline;
  }

  componentDidMount() {
    const { username, follows } = this.props;
    const { isOnline } = this.state;

    this.updateOnlineStatus();

    window.addEventListener('online', this.updateOnlineStatus);
    window.addEventListener('offline', this.updateOnlineStatus);

    if (!follows.length && username && isOnline) {
      actions.loadFollows();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { username, follows } = this.props;
    const { isOnline } = this.state;

    if (prevState.isOnline === isOnline) {
      return;
    }

    if (!follows.length && username && isOnline) {
      actions.loadFollows();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('online', this.updateOnlineStatus);
    window.removeEventListener('offline', this.updateOnlineStatus);
  }

  updateOnlineStatus = () => {
    this.setState({ isOnline: navigator.onLine });
  }

  render() {
    const { classes } = this.props;
    const { isOnline } = this.state;

    return (
      <Fragment>
        <MuiThemeProvider theme={myTheme}>
          {/* {!isOnline && <div className={classes.offline}>offline</div>} */}
          <Settings isOnline={isOnline} />
          {this.isWacherEnabled() && <Watcher />}
        </MuiThemeProvider>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ follows, settings: { username, enabled } }) => ({
  follows,
  username,
  enabled,
});

const App = withStyles(styles)(connect(mapStateToProps)(AppUI));
ReactDOM.render(<Provider><App /></Provider>, document.getElementById('root'));
