const initialState = {
  settings: {
    username: '',
    enabled: false,
    domains: ['domain-me', 'domain-com'],
    interval: 1,
    recentMinutesMin: 20,
    recentMinutesMax: 40,
    hotMinutesMin: 20,
    hotMinutesMax: 40,
    hotSpMin: 50,
  },
  follows: [],
};

export default initialState;
