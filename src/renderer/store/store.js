import createStore from 'react-waterfall';
import initialState from './initial-state';
import { getSettings, setSettings } from '../lib/ls';
import { getFollows as getFollowsApi } from '../../common/api/scorum';

const config = {
  initialState: {
    settings: { ...initialState.settings, ...getSettings() },
    follows: initialState.follows,
    followsForUsername: '',
  },
  actionsCreators: {
    setUsername: async (state, actions, username) => {
      setSettings({ username });

      return {
        ...state,
        settings: {
          ...state.settings,
          username,
        },
      };
    },
    setInputValue: async (state, actions, inputName, value) => {
      setSettings({ [inputName]: value });

      return {
        ...state,
        settings: {
          ...state.settings,
          [inputName]: value,
        },
      };
    },
    setDomains: async (state, actions, domains) => {
      setSettings({ domains });

      return {
        ...state,
        settings: {
          ...state.settings,
          domains,
        },
      };
    },
    toggleEnable: (state, actions, enabled, isCleanFollows) => {
      setSettings({ enabled });

      const newState = {
        ...state,
        settings: {
          ...state.settings,
          enabled,
        },
      };

      if (isCleanFollows) {
        newState.follows = [];
      }

      return newState;
    },
    loadFollows: async (state, actions, username) => {
      const followsForUsername = username || state.settings.username;
      const follows = await getFollowsApi(followsForUsername, 0, 1000);

      return {
        ...state,
        follows: follows.map(item => item.account),
        followsForUsername,
      };
    },
  },
};

export const { Provider, connect, actions } = createStore(config);
