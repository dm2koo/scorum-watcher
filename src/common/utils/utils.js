const Timeago = require('timeago.js');
const differenceInMinutes = require('date-fns/difference_in_minutes');
const { validateAccountName } = require('@scorum/scorum-js/lib/utils');

const timeago = Timeago();

const getWithISOFlag = dateTime => new Date(dateTime.replace(/[zZ]*$/, 'Z'));

exports.timaAgo = dateTime => timeago.format(getWithISOFlag(dateTime));
exports.getDiffMinutes = dateTime => differenceInMinutes(new Date().toISOString(), getWithISOFlag(dateTime));

exports.getVotesCount = post => post.active_votes.filter(vote => vote.percent > 0).length;
exports.getPostId = post => `${post.author}/${post.permlink}`;
exports.getPostSp = post => Math.round(parseFloat(post.pending_payout_sp));
exports.isUserVoted = (votes, username) => votes.some(vote => vote.voter === username);

exports.getPostUrl = (url, domain) => `https://scorum.${domain.replace('domain-', '')}${url.replace('/categories-', '/')}`;

exports.isUsernameValid = username => !validateAccountName(username);
