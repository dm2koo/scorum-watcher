const scorumApi = require('@scorum/scorum-js/lib/api');
const scorumSideApi = require('@scorum/scorum-side-js/lib/api');

exports.getHotPosts = scorumApi.getDiscussionsByHotAsync;
exports.getRecentPosts = scorumApi.getDiscussionsByCreatedAsync;
exports.getAccount = scorumApi.getAccountsAsync;

exports.getFollows = scorumSideApi.getFollowingAsync;
