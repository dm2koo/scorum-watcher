const api = require('@scorum/scorum-js/lib/api');
const config = require('@scorum/scorum-js/lib/config');

let initialized = false;

const initMainConfig = () => {
  api.setOptions({ url: 'https://prodnet.scorum.com' });
  // config.set('chain_id', process.env.CHAIN_ID);
  config.set('address_prefix', 'SCR');
  initialized = true;
};

const isInitialized = () => initialized;

module.exports = {
  initMainConfig,
  isInitialized,
};
