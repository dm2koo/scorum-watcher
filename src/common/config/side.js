const api = require('@scorum/scorum-side-js/lib/api');
const config = require('@scorum/scorum-side-js/lib/config');

let initialized = false;

const initSideConfig = () => {
  api.setOptions({ url: 'https://blog-api.scorum.com' });
  // config.set('chain_id', process.env.CHAIN_ID);
  config.set('address_prefix', 'SCR');
  initialized = true;
};

const isInitialized = () => initialized;

module.exports = {
  initSideConfig,
  isInitialized,
};
