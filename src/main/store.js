const Store = require('electron-store');

const store = new Store();

const SETTINGS_KEY = 'settings';

const getSettings = () => store.get(SETTINGS_KEY);
const setSettings = (settings = {}) => store.set(SETTINGS_KEY, { ...getSettings(), ...settings });
const setSettingsParam = (param, val) => store.set(`${SETTINGS_KEY}.${param}`, val);

const onSettingsChange = cb => store.onDidChange(SETTINGS_KEY, cb);

module.exports = {
  getSettings,
  setSettings,
  setSettingsParam,
  onSettingsChange,
};
