const { app, Menu, nativeImage, Tray } = require('electron');
const path = require('path');
const { IMAGE_ROOT } = require('./constants');
const config = require('./config');

class TrayHandler {
  initTray(win) {
    this.win = win;

    const iconPaths = {
      tray: path.join(IMAGE_ROOT, 'tray', 'tray.png'),
      trayWatching: path.join(IMAGE_ROOT, 'tray', 'tray-watching.png'),
      trayWatchingWithBadge: path.join(IMAGE_ROOT, 'tray', 'tray-watching-badge.png'),
    };

    this.icons = {
      tray: nativeImage.createFromPath(iconPaths.tray),
      trayWatching: nativeImage.createFromPath(iconPaths.trayWatching),
      trayWatchingWithBadge: nativeImage.createFromPath(iconPaths.trayWatchingWithBadge),
    };

    this.tray = new Tray(this.icons.tray);
    this.tray.on('right-click', this.toggleWindow.bind(this));
    this.tray.on('double-click', this.toggleWindow.bind(this));
    this.tray.on('click', this.toggleWindow.bind(this));
    this.tray.setToolTip(config.name);
  }

  toggleWindow() {
    if (this.win.isVisible()) {
      this.win.hide();
    } else {
      this.showWindow();
    }
  }

  showWindow() {
    const position = this.getWindowPosition();
    this.win.setPosition(position.x, position.y, false);
    this.win.show();
    this.win.focus();
  }

  getWindowPosition() {
    const windowBounds = this.win.getBounds();
    const trayBounds = this.tray.getBounds();

    // Center window horizontally below the tray icon
    const x = Math.round(trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2));

    // Position window 4 pixels vertically below the tray icon
    const y = Math.round(trayBounds.y + trayBounds.height + 4);

    return { x, y };
  }

  setIconDefault() {
    this.tray.setImage(this.icons.tray);
  }

  setIconWatching() {
    this.tray.setImage(this.icons.trayWatching);
  }

  setIconWithBadge() {
    this.tray.setImage(this.icons.trayWatchingWithBadge);
  }
}

module.exports = new TrayHandler();
