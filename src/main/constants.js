const path = require('path');

const IMAGE_ROOT = path.join(__dirname, '../..', 'images');

exports.IMAGE_ROOT = IMAGE_ROOT;

exports.PLATFORM = {
  IS_MAC_OS: process.platform === 'darwin',
  IS_WINDOWS: process.platform === 'win32',
  IS_LINUX: process.platform === 'linux',
};

exports.IS_DEV = process.env.NODE_ENV === 'development';
