/* eslint-disable global-require */
const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const debug = require('debug')('main');
const { initMainConfig } = require('../common/config/main');
const { initSideConfig } = require('../common/config/side');
const { IMAGE_ROOT, PLATFORM, IS_DEV } = require('./constants');
const { EVENTS } = require('../common/constants');
const TrayHandler = require('./tray');
const config = require('./config');

const ICON = `icon.${PLATFORM.IS_WINDOWS ? 'ico' : 'png'}`;
const ICON_PATH = path.join(IMAGE_ROOT, 'icon', ICON);

let win;
let isWatching = false;

if (IS_DEV) {
  try { require('electron-reloader')(module); } catch (err) { } // eslint-disable-line no-empty
  require('electron-debug')({ showDevTools: true });
}

initMainConfig();
initSideConfig();

const createWindow = () => {
  win = new BrowserWindow({
    width: config.window.width,
    height: config.window.height,
    title: config.name,
    show: false,
    resizable: false,
    frame: false,
    fullscreenable: false,
    transparent: true,
    icon: ICON_PATH,
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  win.loadURL(path.join('file://', __dirname, '../..', 'index.html'));
  win.setVisibleOnAllWorkspaces(true);

  TrayHandler.initTray(win);

  win.on('show', () => {
    isWatching && TrayHandler.setIconWatching();
  });

  win.on('blur', () => {
    if (!win.webContents.isDevToolsOpened()) {
      win.hide();
    }
  });

  if (IS_DEV) { require('devtron').install(); }
};

// App Events
const handleAppEvents = () => {
  app.dock.hide();

  app.on('ready', createWindow);
  app.on('window-all-closed', () => app.quit());
};

const bindIpcEvents = () => {
  ipcMain.on(EVENTS.WATCHING_STATUS, (e, status) => {
    isWatching = status;
    isWatching ? TrayHandler.setIconWatching() : TrayHandler.setIconDefault();
    debug('Watching status changed: %s', status);
  });

  ipcMain.on(EVENTS.TRAY_ICON_DEFALT, () => {
    TrayHandler.setIconDefault();
  });

  ipcMain.on(EVENTS.TRAY_ICON_WATCHING, () => {
    isWatching && TrayHandler.setIconWatching();
  });

  ipcMain.on(EVENTS.TRAY_ICON_BADGE, () => {
    TrayHandler.setIconWithBadge();
  });

  ipcMain.on(EVENTS.SHOW_WINDOW, () => {
    TrayHandler.showWindow();
  });
};

bindIpcEvents();
handleAppEvents();
