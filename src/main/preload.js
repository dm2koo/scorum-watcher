window.electron = require('electron');
const { ipcRenderer, webFrame } = require('electron');
const { EVENTS } = require('../common/constants');
const { PLATFORM, IS_DEV } = require('./constants');

webFrame.setZoomFactor(1.0);
webFrame.setVisualZoomLevelLimits(1, 1);

window.isMac = PLATFORM.IS_MAC_OS;
window.isWindows = PLATFORM.IS_WINDOWS;
window.isLinux = PLATFORM.IS_LINUX;

const setupIpcInterface = () => {
  window.sendWatchingStatus = (status) => {
    ipcRenderer.send(EVENTS.WATCHING_STATUS, status);
  };

  window.setTrayIconDefault = () => {
    ipcRenderer.send(EVENTS.TRAY_ICON_DEFALT);
  };

  window.setTrayIconWatching = () => {
    ipcRenderer.send(EVENTS.TRAY_ICON_WATCHING);
  };

  window.setTrayIconBadge = () => {
    ipcRenderer.send(EVENTS.TRAY_ICON_BADGE);
  };
};

setupIpcInterface();

if (IS_DEV) {
  window.__devtron = { require, process }; // eslint-disable-line
  localStorage.debug = 'watcher';
}
