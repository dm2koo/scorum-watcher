const pkg = require('../../package.json');

module.exports = {
  name: pkg.productName,
  version: pkg.version,

  window: {
    width: 400,
    height: 600,
  },
};
